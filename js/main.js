//Déclaration et initialisation des variables

let resA = document.querySelectorAll('.A');
let resB = document.querySelectorAll('.B');
let resultat = document.getElementById('resultat');  //recupéeration du id du bouton "voir mes resultats"
let resultat_test = document.getElementById('resultat_test');  //recupéeration du id du champs validation de reponse
// Recuperation des id des différents styles
let styleP = document.getElementById('styleprincipal');
let styleS1 = document.getElementById('stylesecondaire1');
let styleS2 = document.getElementById('stylesecondaire2');
let styleS3 = document.getElementById('stylesecondaire3');
let styleS4 = document.getElementById('stylesecondaire4');
// Recuperation des id des 'box' du graphique
let competitifbox = document.getElementById('competitifid');
let Collaborantbox = document.getElementById('Collaborantid');
let Compromisbox = document.getElementById('Compromisid');
let Evitantbox = document.getElementById('Evitantid');
let Accommodantbox = document.getElementById('Accommodantid');
//Recuperation de tous les éléments qui ont la class '.box' sous forme de tableau
let tailleGraph = document.querySelectorAll('.box');
//Recuperation du id des boutons 'imprimer' et 'sauvegarder'
let imprimer = document.getElementById('print');
let sauvegarder = document.getElementById('sauvegarder');
//variable qui contiennent les scores
let competitif = 0;
let collaborant = 0;
let compromis = 0;
let evitant = 0;
let accommodant = 0;

//fonction qui calcule les scores en fonction des cases cochées dans les questions
function calculerScore() {
    //q1
    if (resA[0].checked) {
        evitant++;
    }
    else {
        accommodant++;
    }
    //q2
    if (resB[1].checked) {
        collaborant++;
    }
    else {
        compromis++;
    }
    //q3
    if (resA[2].checked) {
        competitif++;
    }
    else {
        accommodant++;
    }
    //q4
    if (resA[3].checked) {
        compromis++;
    }
    else {
        accommodant++;
    }
    //q5
    if (resA[4].checked) {
        collaborant++;
    }
    else {
        evitant++;
    }
    //q6
    if (resB[5].checked) {
        competitif++;
    }
    else {
        evitant++;
    }
    //q7
    if (resB[6].checked) {
        compromis++;
    }
    else {
        evitant++;
    }
    //q8
    if (resA[7].checked) {
        competitif++;
    }
    else {
        collaborant++;
    }
    //q9
    if (resB[8].checked) {
        competitif++;
    }
    else {
        evitant++;
    }
    //q10
    if (resA[9].checked) {
        competitif++;
    }
    else {
        compromis++;
    }
    //q11
    if (resA[10].checked) {
        collaborant++;
    }
    else {
        accommodant++;
    }
    //q12
    if (resB[11].checked) {
        compromis++;
    }
    else {
        evitant++;
    }
    //q13
    if (resB[12].checked) {
        competitif++;
    }
    else {
        compromis++;
    }
    //q14
    if (resB[13].checked) {
        competitif++;
    }
    else {
        collaborant++;
    }
    //q15
    if (resB[14].checked) {
        evitant++;
    }
    else {
        accommodant++;
    }

    //q16
    if (resB[15].checked) {
        competitif++;
    }
    else {
        accommodant++;
    }
    //q17
    if (resA[16].checked) {
        compromis++;
    }
    else {
        evitant++;
    }
    //q18
    if (resB[17].checked) {
        compromis++;
    }
    else {
        accommodant++;
    }
    //q19
    if (resA[18].checked) {
        collaborant++;
    }
    else {
        evitant++;
    }
    //q20
    if (resA[19].checked) {
        collaborant++;
    }
    else {
        compromis++;
    }
    //q21
    if (resB[20].checked) {
        collaborant++;
    }
    else {
        accommodant++;
    }
    //q22
    if (resB[21].checked) {
        competitif++;
    }
    else {
        compromis++;
    }
    //q23
    if (resA[22].checked) {
        collaborant++;
    }
    else {
        evitant++;
    }
    //q24
    if (resB[23].checked) {
        compromis++;
    }
    else {
        accommodant++;
    }
    //q25
    if (resA[24].checked) {
        competitif++;
    }
    else {
        accommodant++;
    }
    //q26
    if (resB[25].checked) {
        collaborant++;
    }
    else {
        compromis++;
    }
    //q27
    if (resA[26].checked) {
        evitant++;
    }
    else {
        accommodant++;
    }
    //q28
    if (resA[27].checked) {
        competitif++;
    }
    else {
        collaborant++;
    }

    //q29
    if (resA[28].checked) {
        compromis++;
    }
    else {
        evitant++;
    }

    //q30
    if (resB[29].checked) {
        collaborant++;
    }
    else {
        accommodant++;
    }
}

//Fonction qui calcule les tailles des box calculerTailleBoite()
function calculerTailleBoite() {
    let tabScore = [competitif, collaborant, compromis, evitant, accommodant];
    for (let i = 0; i < tailleGraph.length; i++) {
        tailleGraph[i].style.height = (tabScore[i] * 100) / 11 + '%';
    }
}
//Fonction qui teste si toutes les cases sont cochées ou non et retourne true si oui sinon false
function testCheckedBox() {
    let test;
    for (let i = 0; i < resA.length; i++) {

        if (!resA[i].checked && !resB[i].checked) {
            test = false;
            resA[i].style.backgroundColor = '#A00';
            resultat_test.innerText = 'Question '+[i+1]+' non répondue';
            break;
        }
        else {
            resultat_test.innerText = '';
            test = true;
        }
    }
    return test;
}

//Afficher qui affiche la partie resultat quand on clique sur le bouton 'voir mes résulats'
let hidden = document.querySelector('.hidden');
const afficherPartieResultat = () => {
    hidden.style.display = "block";
};

//Validation des reponses du questionnaire en faisant appel aux fonctions:
// testCheckBox() pour voir toutes les questions sont repondues (si true, exécute le reste des fonctions)
//calculerScore() pour calculer le score en fonction de la case cochée dans chaque question 
//calculerTailleBoite()() pour calculer la taille des box en fonction du score obtenu pour chaque catégorie de profil
//afficherPartieResultat() pour afficher la seconde partie de la page qui contient les reponses

function validerQuestionnaire() {
    //à chaque clic sur le bouton, on initialise les reponses
    competitif = 0;
    collaborant = 0;
    compromis = 0;
    evitant = 0;
    accommodant = 0;
    if (testCheckedBox()) {
        calculerScore();
        calculerTailleBoite();
        afficherPartieResultat();
        // les variables qui contiennent les valeurs des différents scores
        let score1 = competitif;
        let score2 = collaborant;
        let score3 = compromis;
        let score4 = evitant;
        let score5 = accommodant;
        //Affectation des scores en bas des box du graphique
        competitifbox.innerText = competitif;
        Collaborantbox.innerText = collaborant;
        Compromisbox.innerText = compromis;
        Evitantbox.innerText = evitant;
        Accommodantbox.innerText = accommodant;
        //Déclaration et initailisation du tableau pour les labels de l'axe horizontal du graphique
        let stylegestion = [[competitif, "Competitif"], [collaborant, "Collaborant"], [compromis, "Compromis"], [evitant, "Évitant"], [accommodant, "Accommodant"]];
        style(stylegestion);

    }
}

//Fonction ordonne de façon ascendante les scores et les insère dans les 'div' dédiés à l'afficherPartieResultat() des scores
function style(stylegestion) {
    var plus = document.getElementById('r1');
    var moins = document.getElementById('r2');
    stylegestion.sort(sortStyle);           //fait appel à la fonction sortStyle pour trier le tableau
    styleP.innerText = stylegestion[4][1];
    styleS1.innerText = stylegestion[3][1];
    styleS2.innerText = stylegestion[2][1];
    styleS3.innerText = stylegestion[1][1];
    styleS4.innerText = stylegestion[0][1];
    plus.innerText = stylegestion[4][1];    //prendra comme valeur plus grande valeur du tableau 
    moins.innerText = stylegestion[0][1];   //prendra comme valeur plus petite valeur du tableau 
}

//Fonction qui compare a et b et retourne la différence, ici c'est les 1er éléments de notre tableau contiennent les scores
function sortStyle(a, b) {
    return (a[0] - b[0]);
}

// Fonction qui génère et sauvegarde la page en pdf quand on clique sur le bouton 'sauvegarder'
function GenererPdf() {
    var element = document.getElementById('form-print');
    html2pdf().set({
        margin: 1,
        filename: 'Profil Personnel.pdf',
        jsPDF: { unit: 'in' }
    }).from(element).save();
 
}

//Fonction qui imprime les résultats (graphique et score)
function fnimprimer() {
    window.print();
}

//exécution de boutons: resultat, imprimer, sauvegarder
resultat.addEventListener('click', validerQuestionnaire); 
imprimer.addEventListener('click', fnimprimer);           
sauvegarder.addEventListener('click', GenererPdf);        






